def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}

def transformIntoStep(image) {
    return {
      stageStatus = [:]
      try {
        node ('docker') {
            stage("Build docker image for ${image}") {
                sh 'export AWS_PROFILE="uis-generic"'
                sh "docker build -t 957958309439.dkr.ecr.eu-west-2.amazonaws.com/3rdparty/${image}:latest ${image}"
                sh "docker push 957958309439.dkr.ecr.eu-west-2.amazonaws.com/3rdparty/${image}:latest"
                echo "setting stageStatus - SUCCESS"
                stageStatus['status'] = 'SUCCESS'
                //step([$class: 'InfluxDbPublisher',
                //    customData: null,
                //    customDataMap: stageStatus,
                //    customPrefix: "${image}",
                //    target: 'ss-metrics']) 
            }
        }
      } catch (error) {
         echo "setting stageStatus - FAILED"
         stageStatus['status'] = 'FAILED'
      } finally {
             step([$class: 'InfluxDbPublisher',
                customData: stageStatus,
                customDataMap: null,
                customPrefix: "${image}",
                target: 'ss-metrics'])
      }
    }
}

def stepsForParallel = [:]

node ('master || docker') {
    stage('Git checkout') {
        try {
            git credentialsId: '9c7fffc7-7fea-4d52-bb63-3d29c62a69c1', url: 'ssh://ucs@git.csx.cam.ac.uk/automation/container-builds.git'
        } catch (Exception err) {
            echo "Git checkout failed"
        }
    }
}

node ('master') {
        stage('Setup') {
          def files = findFiles(glob: '**/Dockerfile')
          for (ii = 0; ii < files.size(); ii++) {
              sh "echo `dirname ${files[ii].path}`|tr -d '\n' > tmpvar"
              def imageName = readFile 'tmpvar'
              sh "rm tmpvar"
              stepsForParallel[imageName] = transformIntoStep("${imageName}")
          }
        }
  parallel stepsForParallel
}
